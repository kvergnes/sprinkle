#!/usr/bin/env python3

"""Sprinkle - Write random data at random places

Usage:
    sprinkle.py <file> [--size=<size>] [--safe-start=<start>] [--safe-end=<end>] [--once]

Options:
    -h --help               Show this message and exit.
    --version               Show version.
    -s --size=<size>        Maximum sprinkle size, in bytes [default: 32].
    -B --safe-start=<start> Size of header to leave untouched [default: 0].
    -E --safe-end=<end>     Size of footer to leave untouched [default: 0].
    -1 --once               Do a single pass then exit.
"""


import os
from docopt import docopt
from time   import sleep
from random import randbytes, randrange

def sprinkle(args):
    fsize = os.stat(args['<file>']).st_size
    print("File size is %d bytes." %fsize)

    with open(args['<file>'], 'r+b') as f:
        if not args['--once']:
            print("Press Ctrl+C to stop sprinkling...")
        while True:
            pos = randrange(int(args['--safe-start']), fsize - int(args['--safe-end']))
            sz = randrange(1, int(args['--size']))
            f.seek(pos, 0)
            f.write(randbytes(sz))
            print("Wrote %d bytes to 0x%08x." %(sz, pos))
            if args['--once']:
                break
            sleep(1)


if __name__ == '__main__':
    sprinkle(docopt(__doc__, version='Sprinkle 0.1.0'))
